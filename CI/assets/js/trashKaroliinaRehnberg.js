(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [
		{name:"trashKaroliinaRehnberg_atlas_", frames: [[960,1225,958,719],[0,0,958,1582],[960,0,958,1223]]},
		{name:"trashKaroliinaRehnberg_atlas_2", frames: [[0,1210,295,274],[738,0,618,720],[962,722,387,720],[0,722,960,486],[0,0,736,720]]}
];


// symbols:



(lib._13534304411883 = function() {
	this.initialize(ss["trashKaroliinaRehnberg_atlas_"]);
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib._13931643611975 = function() {
	this.initialize(ss["trashKaroliinaRehnberg_atlas_"]);
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib._14024175017080 = function() {
	this.initialize(ss["trashKaroliinaRehnberg_atlas_"]);
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.bag = function() {
	this.initialize(ss["trashKaroliinaRehnberg_atlas_2"]);
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.bottle307639_960_720 = function() {
	this.initialize(ss["trashKaroliinaRehnberg_atlas_2"]);
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.human152644_960_720pngcopy = function() {
	this.initialize(ss["trashKaroliinaRehnberg_atlas_2"]);
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.sand305497_960_720 = function() {
	this.initialize(ss["trashKaroliinaRehnberg_atlas_2"]);
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.thongs308381_960_720 = function() {
	this.initialize(ss["trashKaroliinaRehnberg_atlas_2"]);
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Scene_1_text = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// text
	this.text = new cjs.Text("STOP BEING PART OF THIS", "29px 'Repetition DEMO'", "#FF0000");
	this.text.lineHeight = 45;
	this.text.parent = this;
	this.text.setTransform(268.5,352.9);
	this.text._off = true;

	this.timeline.addTween(cjs.Tween.get(this.text).wait(57).to({_off:false},0).wait(21));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_r4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// r4
	this.instance = new lib.thongs308381_960_720();
	this.instance.parent = this;
	this.instance.setTransform(197.5,196.95,0.0288,0.0288,60.0103);

	this.instance_1 = new lib.thongs308381_960_720();
	this.instance_1.parent = this;
	this.instance_1.setTransform(283.45,231,0.0288,0.0288,104.994);

	this.instance_2 = new lib.thongs308381_960_720();
	this.instance_2.parent = this;
	this.instance_2.setTransform(219,277,0.0288,0.0288);

	this.instance_3 = new lib.bottle307639_960_720();
	this.instance_3.parent = this;
	this.instance_3.setTransform(215,344.9,0.0244,0.0172,150.0471);

	this.instance_4 = new lib.bottle307639_960_720();
	this.instance_4.parent = this;
	this.instance_4.setTransform(266.5,245.05,0.0321,0.0172,105.0232);

	this.instance_5 = new lib.bottle307639_960_720();
	this.instance_5.parent = this;
	this.instance_5.setTransform(251.15,276.85,0.0244,0.0172,150.0471);

	this.instance_6 = new lib.bottle307639_960_720();
	this.instance_6.parent = this;
	this.instance_6.setTransform(214.6,257,0.0244,0.0244,90);

	this.instance_7 = new lib.bottle307639_960_720();
	this.instance_7.parent = this;
	this.instance_7.setTransform(225.9,303.65,0.0244,0.0244,120.0115);

	this.instance_8 = new lib._14024175017080();
	this.instance_8.parent = this;
	this.instance_8.setTransform(190.5,285.8,0.0117,0.0117,14.9218);

	this.instance_9 = new lib._14024175017080();
	this.instance_9.parent = this;
	this.instance_9.setTransform(199.8,311.65,0.0117,0.0117,164.9869);

	this.instance_10 = new lib._14024175017080();
	this.instance_10.parent = this;
	this.instance_10.setTransform(191.85,294,0.0117,0.0117,-15.0657);

	this.instance_11 = new lib._14024175017080();
	this.instance_11.parent = this;
	this.instance_11.setTransform(238.2,317,0.0117,0.0117,119.9021);

	this.instance_12 = new lib._14024175017080();
	this.instance_12.parent = this;
	this.instance_12.setTransform(257.95,255.95,0.0117,0.0117,-15.0657);

	this.instance_13 = new lib._14024175017080();
	this.instance_13.parent = this;
	this.instance_13.setTransform(255.05,260.45,0.0117,0.0117,14.9027);

	this.instance_14 = new lib._14024175017080();
	this.instance_14.parent = this;
	this.instance_14.setTransform(265.5,260.45,0.0117,0.0117,149.9687);

	this.instance_15 = new lib._14024175017080();
	this.instance_15.parent = this;
	this.instance_15.setTransform(218.7,306.2,0.0118,0.0118,14.9553);

	this.instance_16 = new lib._13931643611975();
	this.instance_16.parent = this;
	this.instance_16.setTransform(190,354,0.0118,0.0105);

	this.instance_17 = new lib._13931643611975();
	this.instance_17.parent = this;
	this.instance_17.setTransform(233.5,238.8,0.0118,0.0176,-104.9634);

	this.instance_18 = new lib._13931643611975();
	this.instance_18.parent = this;
	this.instance_18.setTransform(215,323,0.0176,0.0176);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_18},{t:this.instance_17},{t:this.instance_16},{t:this.instance_15},{t:this.instance_14},{t:this.instance_13},{t:this.instance_12},{t:this.instance_11},{t:this.instance_10},{t:this.instance_9},{t:this.instance_8},{t:this.instance_7},{t:this.instance_6},{t:this.instance_5},{t:this.instance_4},{t:this.instance_3},{t:this.instance_2},{t:this.instance_1},{t:this.instance}]},46).to({state:[{t:this.instance_18},{t:this.instance_17},{t:this.instance_16},{t:this.instance_15},{t:this.instance_14},{t:this.instance_13},{t:this.instance_12},{t:this.instance_11},{t:this.instance_10},{t:this.instance_9},{t:this.instance_8},{t:this.instance_7},{t:this.instance_6},{t:this.instance_5},{t:this.instance_4},{t:this.instance_3},{t:this.instance_2},{t:this.instance_1},{t:this.instance}]},31).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_bc2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// bc2
	this.instance = new lib.sand305497_960_720();
	this.instance.parent = this;
	this.instance.setTransform(-28,-11,0.648,0.8453);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(77).to({y:0},0).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.human_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.human152644_960_720pngcopy();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.1529,0.1529);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.human_Layer_1, null, null);


(lib.bc1_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib._13534304411883();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.6857,0.6857);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.bc1_Layer_1, null, null);


(lib.b1_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.bag();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.2931,0.2931);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.b1_Layer_1, null, null);


(lib.human = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1_obj_
	this.Layer_1 = new lib.human_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(29.6,55.1,1,1,0,0,0,29.6,55.1);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 0
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.human, new cjs.Rectangle(0,0,59.2,110.1), null);


(lib.bc1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1_obj_
	this.Layer_1 = new lib.bc1_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(328.4,246.5,1,1,0,0,0,328.4,246.5);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 0
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.bc1, new cjs.Rectangle(0,0,656.9,493), null);


(lib.b1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1_obj_
	this.Layer_1 = new lib.b1_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(43.2,40.1,1,1,0,0,0,43.2,40.1);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 0
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.b1, new cjs.Rectangle(0,0,86.5,80.3), null);


(lib.Scene_1_r = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// r
	this.instance = new lib.b1();
	this.instance.parent = this;
	this.instance.setTransform(220.9,226.15,0.2057,0.2546,0,-75.0051,-82.8882,42.7,40.5);

	this.instance_1 = new lib.b1();
	this.instance_1.parent = this;
	this.instance_1.setTransform(202.65,278.3,0.1506,0.166,0,-119.9925,-127.8867,42.9,40.5);

	this.instance_2 = new lib.b1();
	this.instance_2.parent = this;
	this.instance_2.setTransform(201.35,272,0.148,0.1631,0,-44.9962,-52.8844,42.9,40.9);

	this.instance_3 = new lib.b1();
	this.instance_3.parent = this;
	this.instance_3.setTransform(180.15,288.7,0.1966,0.2166,0,-90,-97.8817,42.9,40.2);

	this.instance_4 = new lib.b1();
	this.instance_4.parent = this;
	this.instance_4.setTransform(219.1,286.55,0.1626,0.1791,0,-45,-52.8892,42.6,40.5);

	this.instance_5 = new lib.b1();
	this.instance_5.parent = this;
	this.instance_5.setTransform(249.55,270.1,0.3017,0.3017,0,0,0,43.3,56.7);

	this.instance_6 = new lib.b1();
	this.instance_6.parent = this;
	this.instance_6.setTransform(208.55,353.1,0.3017,0.3017,120.0001,0,0,43.4,40);

	this.instance_7 = new lib.b1();
	this.instance_7.parent = this;
	this.instance_7.setTransform(205.95,322.65,0.3674,0.4047,0,-120.0002,-127.886,43.1,40.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_7},{t:this.instance_6},{t:this.instance_5},{t:this.instance_4},{t:this.instance_3},{t:this.instance_2},{t:this.instance_1},{t:this.instance}]},46).to({state:[{t:this.instance_7},{t:this.instance_6},{t:this.instance_5},{t:this.instance_4},{t:this.instance_3},{t:this.instance_2},{t:this.instance_1},{t:this.instance}]},31).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_human = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// human
	this.ikNode_1 = new lib.human();
	this.ikNode_1.name = "ikNode_1";
	this.ikNode_1.parent = this;
	this.ikNode_1.setTransform(285.55,247.45,1.7541,1.7541,0,0,0,17,55.4);
	this.ikNode_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.ikNode_1).wait(38).to({_off:false},0).wait(1).to({regX:29.6,regY:55.1,scaleX:1.7499,scaleY:1.7499,x:308.45,y:248},0).wait(1).to({scaleX:1.7457,scaleY:1.7457,x:309.2,y:249.1},0).wait(1).to({scaleX:1.7415,scaleY:1.7415,x:310.05,y:250.15},0).wait(1).to({scaleX:1.7373,scaleY:1.7373,x:310.8,y:251.2},0).wait(1).to({scaleX:1.7331,scaleY:1.7331,x:311.6,y:252.3},0).wait(1).to({scaleX:1.7289,scaleY:1.7289,x:312.45,y:253.35},0).wait(1).to({scaleX:1.7247,scaleY:1.7247,x:313.2,y:254.45},0).wait(1).to({scaleX:1.7205,scaleY:1.7205,x:314,y:255.5},0).wait(1).to({scaleX:1.7163,scaleY:1.7163,x:314.8,y:256.55},0).wait(1).to({scaleX:1.7121,scaleY:1.7121,x:315.6,y:257.65},0).wait(1).to({scaleX:1.7079,scaleY:1.7079,x:316.4,y:258.7},0).wait(1).to({scaleX:1.7037,scaleY:1.7037,x:317.2,y:259.8},0).wait(1).to({scaleX:1.6995,scaleY:1.6995,x:317.95,y:260.85},0).wait(1).to({scaleX:1.6954,scaleY:1.6954,x:318.8,y:261.9},0).wait(1).to({scaleX:1.6912,scaleY:1.6912,x:319.55,y:263},0).wait(1).to({scaleX:1.687,scaleY:1.687,x:320.35,y:264.05},0).wait(1).to({scaleX:1.6828,scaleY:1.6828,x:321.15,y:265.1},0).wait(1).to({scaleX:1.6786,scaleY:1.6786,x:321.95,y:266.2},0).wait(1).to({scaleX:1.6744,scaleY:1.6744,x:322.75,y:267.25},0).wait(1).to({scaleX:1.6702,scaleY:1.6702,x:323.55,y:268.35},0).wait(1).to({scaleX:1.666,scaleY:1.666,x:324.3,y:269.4},0).wait(1).to({scaleX:1.6618,scaleY:1.6618,x:325.15,y:270.45},0).wait(1).to({scaleX:1.6576,scaleY:1.6576,x:325.9,y:271.55},0).wait(1).to({scaleX:1.6534,scaleY:1.6534,x:326.7,y:272.6},0).wait(1).to({scaleX:1.6492,scaleY:1.6492,x:327.5,y:273.65},0).wait(1).to({scaleX:1.645,scaleY:1.645,x:328.3,y:274.75},0).wait(1).to({scaleX:1.6408,scaleY:1.6408,x:329.1,y:275.8},0).wait(1).to({scaleX:1.6366,scaleY:1.6366,x:329.9,y:276.9},0).wait(1).to({scaleX:1.6324,scaleY:1.6324,x:330.65,y:277.95},0).wait(1).to({scaleX:1.6282,scaleY:1.6282,x:331.5,y:279},0).wait(1).to({scaleX:1.6241,scaleY:1.6241,x:332.25,y:280.1},0).wait(1).to({scaleX:1.6199,scaleY:1.6199,x:333.05,y:281.15},0).wait(1).to({scaleX:1.6157,scaleY:1.6157,x:333.85,y:282.2},0).wait(1).to({scaleX:1.6115,scaleY:1.6115,x:334.65,y:283.3},0).wait(1).to({scaleX:1.6073,scaleY:1.6073,x:335.5,y:284.35},0).wait(1).to({scaleX:1.6031,scaleY:1.6031,x:336.25,y:285.45},0).wait(1).to({scaleX:1.5989,scaleY:1.5989,x:337.05,y:286.55},0).wait(1).to({scaleX:1.5947,scaleY:1.5947,x:337.85,y:287.6},0).wait(1).to({scaleX:1.5905,scaleY:1.5905,x:338.65,y:288.7},0).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_bc1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// bc1
	this.instance = new lib.bc1();
	this.instance.parent = this;
	this.instance.setTransform(328.4,168.5,1,1,0,0,0,328.4,246.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({regX:328.5,x:327.15},0).wait(1).to({x:325.75},0).wait(1).to({x:324.35},0).wait(1).to({x:322.95},0).wait(1).to({x:321.6},0).wait(1).to({x:320.2},0).wait(1).to({x:318.8},0).wait(1).to({x:317.4},0).wait(1).to({x:316.05},0).wait(1).to({x:314.65},0).wait(1).to({x:313.25},0).wait(1).to({x:311.85},0).wait(1).to({x:310.5},0).wait(1).to({x:309.1},0).wait(1).to({x:307.7},0).wait(1).to({x:306.3},0).wait(1).to({x:304.9},0).wait(1).to({x:303.55},0).wait(1).to({x:302.15},0).wait(1).to({x:300.75},0).wait(1).to({x:299.35},0).wait(1).to({x:298},0).wait(1).to({x:296.6},0).wait(1).to({x:295.2},0).wait(1).to({x:293.8},0).wait(1).to({x:292.45},0).wait(1).to({x:291.05},0).wait(1).to({x:289.65},0).wait(1).to({x:288.25},0).wait(1).to({x:286.9},0).wait(1).to({x:285.5},0).wait(1).to({x:284.1},0).wait(1).to({x:282.7},0).wait(1).to({x:281.3},0).wait(1).to({x:279.95},0).wait(1).to({x:278.55},0).wait(1).to({x:277.15},0).wait(1).to({x:275.75},0).wait(1).to({x:274.4},0).wait(1).to({x:273},0).wait(1).to({x:271.6},0).wait(1).to({x:270.2},0).wait(1).to({x:268.85},0).wait(1).to({x:267.45},0).wait(1).to({x:266.05},0).wait(1).to({x:264.65},0).wait(1).to({x:263.25},0).wait(1).to({x:261.9},0).wait(1).to({x:260.5},0).wait(1).to({x:259.1},0).wait(1).to({x:257.7},0).wait(1).to({x:256.35},0).wait(1).to({x:254.95},0).wait(1).to({x:253.55},0).wait(1).to({x:252.15},0).wait(1).to({x:250.8},0).wait(1).to({x:249.4},0).wait(1).to({x:248},0).wait(1).to({x:246.6},0).wait(1).to({x:245.25},0).wait(1).to({x:243.85},0).wait(1).to({x:242.45},0).wait(1).to({x:241.05},0).wait(1).to({x:239.65},0).wait(1).to({x:238.3},0).wait(1).to({x:236.9},0).wait(1).to({x:235.5},0).wait(1).to({x:234.1},0).wait(1).to({x:232.75},0).wait(1).to({x:231.35},0).wait(1).to({x:229.95},0).wait(1).to({x:228.55},0).wait(1).to({x:227.2},0).wait(1).to({x:225.8},0).wait(1).to({x:224.4},0).wait(1).to({x:223},0).wait(1).to({x:221.6},0).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_bag = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// bag
	this.instance = new lib.b1();
	this.instance.parent = this;
	this.instance.setTransform(585.3,249.25,1,1,0,0,0,43.5,40.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({regX:43.2,regY:40.1,scaleX:0.9514,scaleY:0.9998,rotation:7.8261,x:577.2,y:256.5},0).wait(1).to({scaleX:0.9027,scaleY:0.9997,rotation:15.6522,x:568.8,y:263.4},0).wait(1).to({scaleX:0.8541,scaleY:0.9995,rotation:23.4783,x:559.8,y:269.6},0).wait(1).to({scaleX:0.8055,scaleY:0.9993,rotation:31.3043,x:550.45,y:275.15},0).wait(1).to({scaleX:0.7568,scaleY:0.9991,rotation:39.1304,x:540.75,y:280.35},0).wait(1).to({scaleX:0.7082,scaleY:0.999,rotation:46.9565,x:531.1,y:285.35},0).wait(1).to({scaleX:0.6596,scaleY:0.9988,rotation:54.7826,x:521.6,y:290.65},0).wait(1).to({scaleX:0.611,scaleY:0.9986,rotation:62.6087,x:512.2,y:296.3},0).wait(1).to({scaleX:0.6109,scaleY:0.905,rotation:70.4348,x:501.05,y:302.7},0).wait(1).to({scaleX:0.6108,scaleY:0.8114,rotation:78.2609,x:489.4,y:308.3},0).wait(1).to({scaleX:0.6107,scaleY:0.7179,rotation:86.087,x:477.55,y:313.35},0).wait(1).to({scaleY:0.6243,rotation:93.913,x:465.5,y:318.05},0).wait(1).to({scaleX:0.6106,scaleY:0.5307,rotation:101.7391,x:453.3,y:322.35},0).wait(1).to({scaleX:0.5761,scaleY:0.5638,rotation:109.5652,x:441.05,y:326.4},0).wait(1).to({scaleX:0.5416,scaleY:0.5969,rotation:117.3913,x:428.7,y:330.05},0).wait(1).to({scaleX:0.5071,scaleY:0.63,rotation:125.2174,x:416.2,y:333.45},0).wait(1).to({scaleX:0.4726,scaleY:0.6631,rotation:133.0435,x:403.65,y:336.5},0).wait(1).to({scaleX:0.4382,scaleY:0.6962,rotation:140.8696,x:391.05,y:339.25},0).wait(1).to({scaleX:0.4037,scaleY:0.7294,rotation:148.6957,x:378.35,y:341.6},0).wait(1).to({scaleX:0.4127,scaleY:0.683,rotation:156.5217,x:365.6,y:343.65},0).wait(1).to({scaleX:0.4217,scaleY:0.6366,rotation:164.3478,x:352.7,y:345.2},0).wait(1).to({scaleX:0.4307,scaleY:0.5902,rotation:172.1739,x:339.9,y:346.4},0).wait(1).to({scaleX:0.4396,scaleY:0.5438,rotation:180,x:326.95,y:347.05},0).wait(1).to({scaleX:0.5157,scaleY:0.5465,rotation:187.8261,x:314.1,y:347.2},0).wait(1).to({scaleX:0.5918,scaleY:0.5491,rotation:195.6522,x:301.2,y:346.75},0).wait(1).to({scaleX:0.6679,scaleY:0.5518,rotation:203.4783,x:288.3,y:345.55},0).wait(1).to({scaleX:0.6323,scaleY:0.6089,rotation:211.3043,x:275.05,y:343.8},0).wait(1).to({scaleX:0.5966,scaleY:0.666,rotation:219.1304,x:261.85,y:341.6},0).wait(1).to({scaleX:0.5609,scaleY:0.7232,rotation:226.9565,x:248.75,y:339.05},0).wait(1).to({scaleX:0.5253,scaleY:0.7803,rotation:234.7826,x:235.65,y:336.2},0).wait(1).to({scaleX:0.4896,scaleY:0.8374,rotation:242.6087,x:222.65,y:333.15},0).wait(1).to({scaleX:0.454,scaleY:0.8945,rotation:250.4348,x:209.7,y:329.9},0).wait(1).to({scaleX:0.4539,scaleY:0.8578,rotation:258.2609,x:196.8,y:326.45},0).wait(1).to({scaleY:0.821,rotation:266.087,x:183.95,y:322.85},0).wait(1).to({scaleY:0.7842,rotation:273.913,x:171.15,y:319.15},0).wait(1).to({scaleX:0.4538,scaleY:0.7475,rotation:281.7391,x:158.4,y:315.25},0).wait(1).to({scaleY:0.7107,rotation:289.5652,x:145.65,y:311.25},0).wait(1).to({scaleX:0.4537,scaleY:0.6739,rotation:297.3913,x:133,y:307.15},0).wait(1).to({scaleY:0.6372,rotation:305.2174,x:120.35,y:302.9},0).wait(1).to({scaleX:0.4536,scaleY:0.6004,rotation:313.0435,x:107.8,y:298.55},0).wait(1).to({scaleY:0.5636,rotation:320.8696,x:95.25,y:294.05},0).wait(1).to({scaleX:0.4535,scaleY:0.5269,rotation:328.6957,x:82.8,y:289.4},0).wait(1).to({scaleY:0.4901,rotation:336.5217,x:70.3,y:284.7},0).wait(1).to({scaleX:0.3211,scaleY:0.3065,rotation:344.3478,x:57.95,y:279.75},0).wait(1).to({scaleX:0.1886,scaleY:0.1229,rotation:352.1739,x:45.7,y:274.7},0).wait(1).to({scaleX:0.0562,scaleY:0.0607,rotation:360,skewX:180,x:33.65,y:269.25},0).wait(1));

}).prototype = p = new cjs.MovieClip();


// stage content:
(lib.trashKaroliinaRehnberg = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	this.___GetDepth___ = function(obj) {
		var depth = obj.depth;
		var cameraObj = this.___camera___instance;
		if(cameraObj && cameraObj.depth && obj.isAttachedToCamera)
		{
			depth += depth + cameraObj.depth;
		}
		return depth;
		}
	this.___needSorting___ = function() {
		for (var i = 0; i < this.getNumChildren() - 1; i++)
		{
			var prevDepth = this.___GetDepth___(this.getChildAt(i));
			var nextDepth = this.___GetDepth___(this.getChildAt(i + 1));
			if (prevDepth < nextDepth)
				return true;
		}
		return false;
	}
	this.___sortFunction___ = function(obj1, obj2) {
		return (this.exportRoot.___GetDepth___(obj2) - this.exportRoot.___GetDepth___(obj1));
	}
	this.on('tick', function (event){
		var curTimeline = event.currentTarget;
		if (curTimeline.___needSorting___()){
			this.sortChildren(curTimeline.___sortFunction___);
		}
	});

	// timeline functions:
	this.frame_38 = function() {
		this.ikNode_1 = this.human.ikNode_1;
	}
	this.frame_77 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(38).call(this.frame_38).wait(39).call(this.frame_77).wait(1));

	// text_obj_
	this.text = new lib.Scene_1_text();
	this.text.name = "text";
	this.text.parent = this;
	this.text.depth = 0;
	this.text.isAttachedToCamera = 0
	this.text.isAttachedToMask = 0
	this.text.layerDepth = 0
	this.text.layerIndex = 0
	this.text.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.text).wait(78));

	// human_obj_
	this.human = new lib.Scene_1_human();
	this.human.name = "human";
	this.human.parent = this;
	this.human.depth = 0;
	this.human.isAttachedToCamera = 0
	this.human.isAttachedToMask = 0
	this.human.layerDepth = 0
	this.human.layerIndex = 1
	this.human.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.human).wait(39).to({regX:320.7,regY:263.2,x:320.7,y:263.2},0).wait(39));

	// r4_obj_
	this.r4 = new lib.Scene_1_r4();
	this.r4.name = "r4";
	this.r4.parent = this;
	this.r4.depth = 0;
	this.r4.isAttachedToCamera = 0
	this.r4.isAttachedToMask = 0
	this.r4.layerDepth = 0
	this.r4.layerIndex = 2
	this.r4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.r4).wait(78));

	// r_obj_
	this.r = new lib.Scene_1_r();
	this.r.name = "r";
	this.r.parent = this;
	this.r.depth = 0;
	this.r.isAttachedToCamera = 0
	this.r.isAttachedToMask = 0
	this.r.layerDepth = 0
	this.r.layerIndex = 3
	this.r.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.r).wait(78));

	// bag_obj_
	this.bag = new lib.Scene_1_bag();
	this.bag.name = "bag";
	this.bag.parent = this;
	this.bag.setTransform(585,249,1,1,0,0,0,585,249);
	this.bag.depth = 0;
	this.bag.isAttachedToCamera = 0
	this.bag.isAttachedToMask = 0
	this.bag.layerDepth = 0
	this.bag.layerIndex = 4
	this.bag.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.bag).wait(1).to({regX:329.7,regY:293.9,x:329.7,y:293.9},0).wait(45).to({_off:true},1).wait(31));

	// bc2_obj_
	this.bc2 = new lib.Scene_1_bc2();
	this.bc2.name = "bc2";
	this.bc2.parent = this;
	this.bc2.setTransform(283,194.4,1,1,0,0,0,283,194.4);
	this.bc2.depth = 0;
	this.bc2.isAttachedToCamera = 0
	this.bc2.isAttachedToMask = 0
	this.bc2.layerDepth = 0
	this.bc2.layerIndex = 5
	this.bc2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.bc2).wait(78));

	// bc1_obj_
	this.bc1 = new lib.Scene_1_bc1();
	this.bc1.name = "bc1";
	this.bc1.parent = this;
	this.bc1.setTransform(328.4,168.5,1,1,0,0,0,328.4,168.5);
	this.bc1.depth = 0;
	this.bc1.isAttachedToCamera = 0
	this.bc1.isAttachedToMask = 0
	this.bc1.layerDepth = 0
	this.bc1.layerIndex = 6
	this.bc1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.bc1).wait(1).to({regX:275,x:275},0).wait(77));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(168.1,122,488.79999999999995,309.7);
// library properties:
lib.properties = {
	id: '34476AD2149D0A4B99CADBF11F1A68F7',
	width: 550,
	height: 400,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"/CI/assets/img/trashKaroliinaRehnberg_atlas_.png", id:"trashKaroliinaRehnberg_atlas_"},
		{src:"/CI/assets/img/trashKaroliinaRehnberg_atlas_2.png", id:"trashKaroliinaRehnberg_atlas_2"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['34476AD2149D0A4B99CADBF11F1A68F7'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


// Layer depth API : 

AdobeAn.Layer = new function() {
	this.getLayerZDepth = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth; else 0;";
		return eval(script);
	}
	this.setLayerZDepth = function(timeline, layerName, zDepth)
	{
		const MAX_zDepth = 10000;
		const MIN_zDepth = -5000;
		if(zDepth > MAX_zDepth)
			zDepth = MAX_zDepth;
		else if(zDepth < MIN_zDepth)
			zDepth = MIN_zDepth;
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth = " + zDepth + ";";
		eval(script);
	}
	this.removeLayer = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline.removeChild(timeline." + layerName + ");";
		eval(script);
	}
	this.addNewLayer = function(timeline, layerName, zDepth)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		zDepth = typeof zDepth !== 'undefined' ? zDepth : 0;
		var layer = new createjs.MovieClip();
		layer.name = layerName;
		layer.depth = zDepth;
		layer.layerIndex = 0;
		timeline.addChild(layer);
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;