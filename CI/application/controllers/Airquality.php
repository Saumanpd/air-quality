<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require '../vendor/autoload.php';
use GuzzleHttp\Client;
define ('REST_SERVER','https://api.waqi.info/feed/');
define('REST_SERVER_API_TOKEN','bd4040f1f84d1a2eb9f0ccaaf01a83fc0471cb5c');

/*
 * Class of Airquality controller
 *
 * @author: Petr Sauman, Pekka Keranen
 */
class Airquality extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
    }
    
   /*
    * Function returns main page with header and footer. Headline is included in Header.
    * @param: - 
    * @return: -
    * @author: Petr Sauman
    */  
    public function index() {
        $data['headline'] = "Main page";
        $this->load->view('templates/header', $data);
        $this->load->view('index');
        $this->load->view('templates/footer');
    }
    
   /*
    * Function returns result of information about city from REST server.
    * @param: - 
    * @return: -
    * @author: Petr Sauman
    */ 
    public function chosencity() {
         $this->form_validation->set_rules('city', 'City', 'trim|required');
         $city=$this->input->post('city');
        $client = new Client ([
            'base_uri' => REST_SERVER,
            'timeout' => 2.0
            ]);
        $token = 'bd4040f1f84d1a2eb9f0ccaaf01a83fc0471cb5c';
        
        if ($city == NULL) {
            $data['headline'] = "Write the name of city";
            $this->load->view('templates/header', $data);
            $this->load->view('index');
            $this->load->view('templates/footer');    
        } else {
            $response = $client->get(REST_SERVER . $city .'/?token=' . $token );
            $data['climate'] = json_decode($response->getBody(), true);
            $data['headline'] = "Air quality in " . $city;
            $this->load->view('templates/header', $data);
            $this->load->view('city/index', $data);
            $this->load->view('templates/footer');
        }
    }

        /*
         * Function calls webpage with animation and header with footer.
         * @param: - 
         * @return: -
         * @author: Pekka Keranen
         */ 
        public function animation() {
        $data['headline'] = "Stop being part of this";
        $this->load->view('templates/header', $data);
        $this->load->view('animation/index');
        $this->load->view('templates/footer');
    }
}