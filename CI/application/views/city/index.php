<!--
 * Result page of information about city from RESTSERVER
 *
 * @author: Petr Sauman, Pekka Keranen
 -->
<div class="container">
<div class="row">
<div class="col">
    
     <p class="text-info"><?php echo 'Status: ' . $climate['status']; ?></p>
     <p class="text-info"><?php echo 'Exactly location is - ' . $climate['data']['city']['name']; ?> </p>
     <p class="text-info"><?php echo "<a href='" . $climate['data']['city']['url'] . "'>Link to this location with data</a>"; ?> </p>
     <p class="text-info"><?php echo 'Data (aqi): ' . $climate['data']['aqi']; ?></p>
     <p class="text-info"><?php echo 'Data (idx): ' . $climate['data']['idx']; ?></p>
    

    <h3 align="center" class="text-info">About API:</h3>
    <?php foreach($climate['data']['attributions'] as $attributions); ?>
        <p class="text-info"><?php echo 'Name of gauging station: ' . $attributions['name']; ?></p>
        <p class="text-info"><?php echo "<a href='" . $attributions['url'] . "'>Link to website</a>"; ?></p>
    <php endforeach; ?>
    </br>
    <p align="center">
    <?php print anchor('Airquality/', "Back to main page", "class='btn btn-primary'"); ?>
     </p>
     
</div>
</div>
</div>