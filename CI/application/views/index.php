<!--
 * Index page (Main page)
 *
 * @author: Petr Sauman, Pekka Keranen
 -->
<div class="container">
    <div class="row">
        <div class="col-md-6">
            
    <?php echo validation_errors(); ?>
    <?php echo form_open_multipart('airquality/chosencity',['class' =>'form-inline']); ?>

    <label for="city" class="text-info">City name: </label> &nbsp; 
    <input type="city" name="city" id="city" class="form-control" placeholder="Example">&nbsp;
    <input type="submit" value="Search" id="submit" name="submit" class="btn btn-primary">
    <?php echo form_close(); ?>

    <?php echo form_open_multipart('airquality/animation'); ?>
    <input type="submit" value="This is our project animation" id="submit" name="submit" class="btn btn-primary">
    <?php echo form_close(); ?>
        </div>
        
    </div>
    
<div class="row">
<div class="col-md-6 p-2">
    
</style>
<script  type="text/javascript"  charset="utf-8">
/** 
 * Filefunctions.js 
 * Widget describing information about air quality in OUlu
 * Authors Petr Sauman, Pekka Keranen 
*/ 
    (function(w,d,t,f){  w[f]=w[f]||function(c,k,n){s=w[f],k=s['k']=(s['k']||(k?('&k='+k):''));s['c']=  
    c=(c  instanceof  Array)?c:[c];s['n']=n=n||0;L=d.createElement(t),e=d.getElementsByTagName(t)[0];  
    L.async=1;L.src='//feed.aqicn.org/feed/'+(c[n].city)+'/'+(c[n].lang||'')+'/feed.v1.js?n='+n+k;  
    e.parentNode.insertBefore(L,e);  };  })(  window,document,'script','_aqiFeed'  );    
</script>
<span  id="city-aqi-container" class="text-info"></span>
  
<script  type="text/javascript"  charset="utf-8">  
    _aqiFeed({  container:"city-aqi-container",  city:"Oulu"  });  
</script>



<div  id='map'  style='height:500px; width:100%; border: 3px solid black; border-radius: 8px; padding: 100px;'  />  
<script  src="https://maps.googleapis.com/maps/api/js"></script>  
  
<script> 
/** 
 * Filefunctions.js 
 * Map function
 * Authors Petr Sauman, Pekka Keranen 
*/ 
      var  map  =  new  google.maps.Map(document.getElementById('map'),  {  
                  center:  new  google.maps.LatLng(65.012093,  25.465076),  
                  mapTypeId:  google.maps.MapTypeId.terrain,  
                  zoom:  6
              });  
  
      var  t  =  new  Date().getTime();  
      var  waqiMapOverlay  =  new  google.maps.ImageMapType({  
                  getTileUrl:  function(coord,  zoom)  {  
                        return  'https://tiles.waqi.info/tiles/usepa-aqi/'  +  zoom  +  "/"  +  coord.x  +  "/"  +  coord.y  +  ".png?token=bd4040f1f84d1a2eb9f0ccaaf01a83fc0471cb5c";  
                  },  
                  name:  "Air  Quality",  
      });  
      map.overlayMapTypes.insertAt(0,waqiMapOverlay);  
</script>

</div>
</div>
    <div class="col-md-6 p-2">
    <img src="<?php echo base_url('assets/img/table.PNG');?>" alt="Image wasnt loaded" style="border:3px solid black; border-radius: 8px; width: 100%;"
    </div>

</div>
<div class="row">
       
        
    <div class="col-md-6 p-2">
        <iframe width="700px" height="500px" src="https://www.youtube.com/embed/Y9vltmBQHgM" 
        frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
    
</div>

</div>
