<!--
 * Header of every page with h2 which has to be set in Controller
 *
 * @author: Petr Sauman, Pekka Keranen
 -->
   <html>
       <head>
           <title> Air quality APP</title>
                   <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
                   <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
                   <link href="<?php echo base_url('assets/css/style.css');?>" rel="stylesheet">
                   <meta name="viewport" content="width=device-width, initial-scale=1">
       </head>
   <body>
     
         
              <h1 class="text-info"> Air quality application</h1>
            <h2 class="text-info"><?php echo$headline; ?></h2>
        